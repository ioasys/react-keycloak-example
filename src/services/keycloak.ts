import Keycloak from 'keycloak-js';

const keycloak = Keycloak({
  realm: 'test-realm',
  url: 'https://app-basf-sso-dev.azurewebsites.net/',
  clientId: 'test-client:3000'
});

export default keycloak;
