import { ThemeProvider as StyledThemeProvider } from 'styled-components';

interface ThemeProviderProps {
  children?: React.ReactChild | React.ReactChild[];
}

const ThemeProvider = ({ children }: ThemeProviderProps) => (
  <StyledThemeProvider theme={null}>{children}</StyledThemeProvider>
);

export default ThemeProvider;
