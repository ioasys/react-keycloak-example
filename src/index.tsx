import React from 'react';
import ReactDOM from 'react-dom';

import { ReactKeycloakProvider } from '@react-keycloak/web';

import Router from './routes/router';
import GlobalStyles from './styles/global';
import keycloak from './services/keycloak';

ReactDOM.render(
  <React.StrictMode>
    <ReactKeycloakProvider authClient={keycloak}>
      <GlobalStyles />
      <Router />
    </ReactKeycloakProvider>
  </React.StrictMode>,
  document.getElementById('root')
);
