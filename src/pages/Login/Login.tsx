import { useKeycloak } from '@react-keycloak/web';
import { useEffect } from 'react';
import { useNavigate } from 'react-router-dom';

import * as S from './Login.styles';

const Login = () => {
  const { keycloak } = useKeycloak();
  const navigate = useNavigate();

  const isLoggedIn = keycloak.authenticated;
  useEffect(() => {
    if (!isLoggedIn) {
      keycloak.login({ redirectUri: 'http://localhost:3000/home' });
    } else {
      navigate('home');
    }
  }, []);

  return <S.Wrapper></S.Wrapper>;
};
export default Login;
