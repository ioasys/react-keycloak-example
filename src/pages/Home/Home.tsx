import { useKeycloak } from '@react-keycloak/web';

const Home = () => {
  const { keycloak } = useKeycloak();
  return (
    <div>
      <p>Homepage</p>
      <button onClick={() => keycloak.accountManagement()}>Editar conta</button>
      <button onClick={() => keycloak.logout({ redirectUri: 'http://localhost:3000/' })}>
        Logout
      </button>
    </div>
  );
};

export default Home;
